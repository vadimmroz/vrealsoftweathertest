import {configureStore} from "@reduxjs/toolkit";
import citySlice from "./slices/citySlice.js";
import currentLocationSlice from "./slices/currentLocationSlice.js";

export default configureStore({
    reducer: {
        city: citySlice,
        currentLocation: currentLocationSlice
    }
})