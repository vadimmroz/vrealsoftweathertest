import {createSlice} from "@reduxjs/toolkit";

const citySlice = createSlice({
    name: "city",
    initialState: {
        city: []
    },
    reducers: {
        setCity(state, action) {
            state.city = action.payload
        },
        addCity(state, action) {
            state.city.push(action.payload)
            localStorage.setItem("city", JSON.stringify(state.city))
        },
        deleteCity(state, action) {
            state.city = state.city.filter(e => e !== action.payload)
            localStorage.setItem("city", JSON.stringify(state.city))
        }
    }
})

export const {setCity, addCity, deleteCity} = citySlice.actions
export default citySlice.reducer