import axios from "axios";
import {
    ApiKey,
    url,
    forecastUrlByCoords,
    forecastUrlByCityName,
    weatherUrlByCityName,
    weatherUrlByCoords, CitiesUrl, ApiKeyCities
} from "./config.js";
import {fetchWeatherApi} from 'openmeteo';
import moment from "moment";


export const getArchiveWeather = async (lat, lon) => {
    const params = {
        "latitude": lat,
        "longitude": lon,
        "start_date": moment().subtract(10, "days").format("YYYY-MM-DD"),
        "end_date": moment().format("YYYY-MM-DD"),
        "daily": "weather_code"
    };
    return await fetchWeatherApi(url, params)
}

export const getForecastByCityName = async (city, code) => {
    return await axios.get(forecastUrlByCityName + city + `,${code}` + "&appid=" + ApiKey)
}
export const getForecastByCoords = async ({lat, lon}) => {
    return await axios.get(forecastUrlByCoords + `lat=${lat}&lon=${lon}` + "&appid=" + ApiKey)
}
export const getWeatherByCityName = async (city, code) => {
    return await axios.get(weatherUrlByCityName + city + `,${code}` + "&appid=" + ApiKey)
}
export const getWeatherByCoords = async ({lat, lon}) => {
    return await axios.get(weatherUrlByCoords + `lat=${lat}&lon=${lon}` + "&appid=" + ApiKey)
}

export const getCites = async (name) => {
    return await axios.get(CitiesUrl + `${name}&limit=5`, {
        headers: {'X-Api-Key': ApiKeyCities}
    })
}