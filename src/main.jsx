import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import {Provider} from "react-redux";
import store from "./store/store.js";
import {QueryClient, QueryClientProvider} from "react-query";
import "./translate/i18n.js"
import {Suspense} from "react";
import Preloader from "./components/preloader/Preloader.jsx";

const queryClient = new QueryClient({
    keepPreviousData: true,
    refetchOnWindowFocus: false
})

ReactDOM.createRoot(document.getElementById('root')).render(
    <Provider store={store}>
        <QueryClientProvider client={queryClient}>
            <Suspense fallback={<Preloader/>}>
                <App/>
            </Suspense>
        </QueryClientProvider>
    </Provider>,
)
