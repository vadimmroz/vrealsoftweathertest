import Header from "./components/header/Header.jsx";
import Home from "./components/home/Home.jsx";
import openWeather from "./assets/openweather.jpg"


const App = () => {
    return <>
        <Header/>
        <Home/>
        <a href="https://openweathermap.org" className="img"><img  src={openWeather} alt=""/></a>
    </>
}

export default App
