import {useEffect} from 'react';
import classes from "./home.module.scss"
import {useDispatch, useSelector} from "react-redux";
import {setCity} from "../../store/slices/citySlice.js";
import {setCurrentLocation} from "../../store/slices/currentLocationSlice.js";
import City from "../city/City.jsx";
import CurrentCity from "../currentCity/CurrentCity.jsx";

const Home = () => {
    const dispatch = useDispatch()
    const city = useSelector(state => state.city.city)
    const current = useSelector(state => state.currentLocation.currentLocation)

    useEffect(() => {
        if (localStorage.getItem("city")) {
            const a = (JSON.parse(localStorage.getItem("city")))
            dispatch(setCity(a))
        }
    }, [])

    useEffect(() => {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                dispatch(setCurrentLocation({
                    lat: position.coords.latitude,
                    lon: position.coords.longitude
                }))
            })
        }
    }, [])
    return (
        <div className={classes.home}>
            <CurrentCity coords={current}/>
            {city?.map((e, i) => {
                return <City element={e} key={i}/>
            })}
        </div>
    );
};

export default Home;