import classes from "../city/city.module.scss"
import {useQuery} from "react-query";
import {getForecastByCoords, getWeatherByCoords} from "../../api/api.js";
import {useEffect, useState} from "react";
import moment from "moment";
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import {Line} from 'react-chartjs-2';
import {useTranslation} from "react-i18next";

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);


const City = ({coords}) => {

    const {t} = useTranslation()

    const weather = useQuery(["currentCityWeather", coords], () => getWeatherByCoords(coords), {
        keepPreviousData: true,
        refetchOnWindowFocus: false
    })
    const forecast = useQuery(["currentCityForecast", coords], () => getForecastByCoords(coords), {
        keepPreviousData: true,
        refetchOnWindowFocus: false
    })

    const [regime, setRegime] = useState(true)
    const [data, setData] = useState({
        labels: [],
        datasets: [
            {
                label: '',
                data: [],
                borderColor: 'rgb(255, 99, 132)',
                backgroundColor: 'rgba(255, 99, 132, 0.5)'
            }
        ],
    })

    const options = {
        responsive: false,
    };

    useEffect(() => {
        if (forecast?.data?.data && !data.labels.length) {
            const labels = forecast.data?.data.list.map(e => moment(e.dt).format("mm:ss")).filter((e, i) => i % 5 === 0);
            const dataset = forecast.data?.data.list.map(e => Number(e.main.temp - 273.15).toFixed(0)).filter((e, i) => i % 5 === 0);
            setData({
                datasets: [
                    {
                        ...data.datasets[0],
                        data: dataset
                    }
                ],
                labels: labels
            })
        }
    }, [forecast, data.labels]);

    const handleRegime = (e) => {
        setRegime(e)
    }

    let weatherIcons = {
        "Clear": 'https://i.ibb.co/ZfkF23t/clear-1.png',
        "Clouds": 'https://i.ibb.co/Mf1s79H/clouds-2.png',
        "Rain": 'https://i.ibb.co/kQKN7xw/free-icon-rain-8722103.png',
        "Drizzle": "https://i.ibb.co/vjc3WbT/4837678.png",
        "Snow": "https://i.ibb.co/R2L5bbr/1247782.png"
    }

    return (
        <>{weather.isSuccess &&
            <div className={classes.city}>
                <div className={classes.cityHeader}>
                    <h2>{weather?.data?.data?.name}, {weather?.data?.data?.sys?.country}</h2>
                    <div>
                        <img src={weather.isSuccess ? weatherIcons[weather?.data?.data.weather[0].main] : ""} alt=""/>
                        <p> {t("city.weather." + weather?.data?.data?.weather[0].main)}</p>
                    </div>
                </div>
                <div className={classes.today}>
                    <h3>{t("city.date." + moment().format("ddd")) + ", " + moment().format("DD ") + t("city.date." + moment().format("MMMM")) + ", " + moment().format("HH:mm")}</h3>
                </div>
                <div className={classes.chart}>
                    <Line options={options} data={data}/>
                </div>
                <div className={classes.weather}>
                    <div className={classes.main}>
                        <div className={classes.temp}>
                            <h1>{regime ? (Number(weather.data?.data.main.temp - 273.15 || 0).toFixed(0)) : (Number(((weather.data?.data.main.temp - 273.15 || 0) * 9 / 5) + 32).toFixed(0))}</h1>
                            <div>
                                <button onClick={() => handleRegime(true)} className={regime ? classes.active : ""}>°C
                                </button>
                                <button onClick={() => handleRegime(false)} className={!regime ? classes.active : ""}>°F
                                </button>
                            </div>
                        </div>
                        <p className={classes.feelsLike}>
                            {t("city.weather.Feels like")} :<span>{regime ? (Number(weather.data?.data.main.feels_like - 273.15 || 0).toFixed(0)) : (Number(((weather.data?.data.main.feels_like - 273.15 || 0) * 9 / 5) + 32).toFixed(0))} {regime ? "°C" : "°F"}</span>
                        </p>
                    </div>
                    <div className={classes.statistic}>
                        <p>{t("city.weather.Wind")}: <span>{weather.data?.data.wind.speed.toFixed(1)}{t("city.weather.m/s")}</span>
                        </p>
                        <p>{t("city.weather.Humidity")}: <span>{weather.data?.data.main.humidity}%</span></p>
                        <p>{t("city.weather.Pressure")}: <span>{weather.data?.data.main.pressure}{t("city.weather.Pa")}</span>
                        </p>
                    </div>
                </div>
            </div>
        }</>
    );
};

export default City;