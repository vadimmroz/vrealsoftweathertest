import classes from "./preloader.module.scss"

const Preloader = () => {
    return (
        <div className={classes.preloader}>
            <div/>
        </div>
    );
};

export default Preloader;