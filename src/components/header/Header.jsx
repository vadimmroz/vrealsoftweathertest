import {useTranslation} from 'react-i18next';
import classes from "./header.module.scss"
import {useEffect, useState} from "react";
import {getCites, getWeatherByCityName} from "../../api/api.js";
import {useDispatch} from "react-redux";
import {addCity} from "../../store/slices/citySlice.js";

const Header = () => {
        const [input, setInput] = useState("")
        const [list, setList] = useState([])
        const [isListVisible, setIsListVisible] = useState(true)
        const [language, setLanguage] = useState("en")
        const dispatch = useDispatch()
        useEffect(() => {
            if (localStorage.getItem("i18nextLng")) {
                setLanguage(localStorage.getItem("i18nextLng"))
            }
        }, []);
        const changeLanguage = (language) => {
            i18n.changeLanguage(language.target.value)
            setLanguage(language.target.value)
        }

        const handleInput = e => {
            if (!isListVisible) {
                setIsListVisible(true)
            }
            setInput(e.target.value)
            if (e.target.value.length > 0) {
                getCites(e.target.value).then(res => {
                        console.log(res)
                        if (e.target.value.length > 0) {
                            const arr = res?.data.map(e => {
                                return {city: e.name, country: e.country}
                            })
                            setList(arr)
                        } else {
                            setList([])
                        }
                    }
                )
            } else {
                setList([])
            }
        }
        const handleAdd = async () => {
            if (input.length > 3) {
                console.log(1)
                await getWeatherByCityName(input).then(res => {
                    if (res) {
                        dispatch(addCity(input))
                        setInput("")
                    }
                }).catch(err => console.log(err))
            }
        }
        const handleLanguage = (e) => {
            setInput(e.city)
            setList([])
            setIsListVisible(false)
        }

        const {t, i18n} = useTranslation();
        return (
            <header className={classes.header}>
                <div className={classes.language}>
                    <form>
                        <select value={language} onChange={changeLanguage}>
                            <option value="en">EN</option>
                            <option value="ua">UA</option>
                            <option value="he">HE</option>
                        </select>
                    </form>
                </div>
                <div className={classes.search}>
                    <input type="text" value={input} onChange={handleInput} placeholder={t("search")}/>
                    <button onClick={handleAdd}>{t("add")}</button>
                </div>
                {isListVisible && <div className={classes.list}>
                    {list?.map((e, i) => {
                        return <button onClick={() => handleLanguage(e)} key={i}>{e.city} {e.country}</button>
                    })}
                </div>}
            </header>
        );
    }
;

export default Header;